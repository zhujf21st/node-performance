// hello.js
const addon = require('./build/Release/addon');

// C++ 计算
console.log(new Date().toLocaleString());
console.log('The C++ result is: ', addon.fib(45));
console.log(new Date().toLocaleString());


// JavaScript 计算
function fib(v) {
	if (v == 0) return 0;
	else if (v == 1) return 1;
	else return fib(v - 1) + fib(v - 2);
}

console.log('The js result is: ', fib(45));
console.log(new Date().toLocaleString());